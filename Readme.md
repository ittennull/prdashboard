## PrDashbord

This app provides a dashboard that shows a list of pull requests of selected projects from Azure DevOps.
Azure DevOps already has similar thing but it's not possible to see pull requests from different projects because every project opens its own window with a list. It's not comfortable to always jump between projects or windows if you want to see everything.