namespace PrDashboard.Server

open System
open System.Security.Claims
open System.Threading.Tasks
open System.Net.Http
open Microsoft.AspNetCore
open Microsoft.AspNetCore.Authentication
open Microsoft.AspNetCore.Authentication.Cookies
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Hosting
open Microsoft.AspNetCore.Http
open Microsoft.Extensions.Configuration
open Microsoft.Extensions.DependencyInjection
open Microsoft.Extensions.Hosting
open Blazored.LocalStorage
open Bolero.Remoting.Server
open Bolero.Server
open Bolero.Templating.Server
open Microsoft.Extensions.Options
open PrDashboard.Server.Types.Cache
open PullRequestService
open PrDashboard.Server.Types
open PrDashboard.Server.Cache
open PrDashboard.Server.Auth

type Startup(configuration: IConfiguration) =
    let addFunctions (services: IServiceCollection) =
        let bindArg ((sp: IServiceProvider), f) =
            let arg = sp.GetRequiredService<_>()
            sp, f arg
            
        let bindOptions (sp, f) =
            let f' (x: IOptions<_>) = f x.Value
            bindArg (sp, f')
        
        let bindHttpClient (sp, f) =
            let f' (x: IHttpClientFactory) = f (x.CreateClient())
            bindArg (sp, f')
            
        let take f sp = sp, f
        let get f = snd >> f
        let addSingleton (f: _ -> 'a) = services.AddSingleton<'a>(f) |> ignore
            
        addSingleton (take saveToken >> bindArg >> get Cache.SaveToken)
        addSingleton (take getToken >> bindArg >> get Cache.GetToken)
        addSingleton (take saveAvatar >> bindArg >> get Cache.SaveAvatar)
        addSingleton (take getAvatar >> bindArg >> get Cache.GetAvatar)
        addSingleton (take getLoginUrl >> bindOptions >> get Auth.GetLoginUrl)
        addSingleton (take getTokenByCode >> bindOptions >> bindHttpClient >> get Auth.GetTokenByCode)
        addSingleton (take refreshToken >> bindOptions >> bindHttpClient >> get Auth.RefreshToken)
        
    member this.ConfigureServices(services: IServiceCollection) =
        services.AddOptions().Configure<OAuth>(configuration.GetSection "OAuth") |> ignore
        services.AddMvc() |> ignore
        services.AddControllers() |> ignore
        services.AddHttpClient() |> ignore
        services.AddMemoryCache() |> ignore
        addFunctions services
        services
            .AddAuthorization()
            .AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(fun opt ->
                    opt.Events.OnValidatePrincipal <- fun context -> 
                        let (GetToken getToken) = context.HttpContext.RequestServices.GetRequiredService<GetToken>()
                        let sessionId = context.Principal.FindFirst(CustomClaims.sessionId).Value
                        match getToken sessionId with
                        | Some token ->
                            context.Principal.Identities
                            |> Seq.head
                            |> fun x -> x.AddClaims([
                                Claim(CustomClaims.accessToken, token.AccessToken)
                                Claim(CustomClaims.refreshToken, token.RefreshToken) ])
                            Task.CompletedTask
                        | None ->
                            context.RejectPrincipal()
                            context.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme)
                    opt.LoginPath <- PathString("/login")
                    opt.LogoutPath <- PathString("/logout"))
                .Services
            .AddBoleroRemoting<PullrequestService>()
            .AddBoleroHost()
            .AddBlazoredLocalStorage()
#if DEBUG
            .AddHotReload(templateDir = __SOURCE_DIRECTORY__ + "/../PrDashboard.Client")
#endif
        |> ignore

    member this.Configure(app: IApplicationBuilder, env: IWebHostEnvironment) =
        if env.IsDevelopment() then
            app.UseWebAssemblyDebugging()
            
        app
            .UseAuthentication()
            .UseStaticFiles()
            .UseRouting()
            .UseAuthorization()
            .UseBlazorFrameworkFiles()
            .UseEndpoints(fun endpoints ->
#if DEBUG
                endpoints.UseHotReload()
#endif
                endpoints.MapControllers() |> ignore
                endpoints.MapBoleroRemoting() |> ignore
                endpoints.MapFallbackToBolero(Index.page) |> ignore)
        |> ignore

module Program =

    [<EntryPoint>]
    let main args =
        WebHost
            .CreateDefaultBuilder(args)
            .UseSetting(WebHostDefaults.DetailedErrorsKey, "true")
            .UseStaticWebAssets()
            .UseStartup<Startup>()
            .Build()
            .Run()
        0
