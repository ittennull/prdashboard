module PrDashboard.Server.Cache

open Microsoft.Extensions.Caching.Memory
open PrDashboard.Server.Types

module private Implementation =
    let save (cache: IMemoryCache) (id: string) value =
        cache.Set(id, value) |> ignore
        
    let get<'T> (cache: IMemoryCache) (id: string) =
        let found, value = cache.TryGetValue<'T> id
        if found then Some value
        else None
    
let saveToken cache id (token: Token) =
    Implementation.save cache id token
    
let getToken cache id =
    Implementation.get<Token> cache id
    
let saveAvatar cache id (avatar: Avatar) =
    Implementation.save cache id avatar
    
let getAvatar cache id =
    Implementation.get<Avatar> cache id
