module PrDashboard.Server.Index

open Bolero.Html
open Bolero.Server.Html
open PrDashboard

let page = doctypeHtml {
    head {
        meta { attr.charset "UTF-8" }
        meta { attr.name "viewport"; attr.content "width=device-width, initial-scale=1.0" }
        title { "PR dashboard" }
        ``base`` { attr.href "/" }
        link { attr.rel "stylesheet"; attr.href "https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css" }
        link { attr.rel "stylesheet"; attr.href "//cdn.materialdesignicons.com/5.4.55/css/materialdesignicons.min.css" }
        link { attr.rel "stylesheet"; attr.href "css/index.css" }
        link { attr.rel "shortcut icon"; attr.href "logo.png" }
    }
    body {
        attr.``class`` "px-2 py-2"
        div { attr.id "main"; comp<Client.Main.MyApp> }
        boleroScript
    }
}
