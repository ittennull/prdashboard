namespace PrDashboard.Server.Controllers.AvatarController

open Microsoft.AspNetCore.Mvc
open PrDashboard.Server.Types

[<ApiController>]
type AvatarController(getAvatar: Cache.GetAvatar) =
    inherit ControllerBase()
    
    let (Cache.GetAvatar _getAvatar) = getAvatar
    
    [<HttpGet("/avatar/{avatarId}")>]    
    member this.Get(avatarId: string) =
        let avatar = _getAvatar avatarId
        match avatar with
        | Some { Bytes = bytes } -> this.File(bytes, "image/png") :> IActionResult
        | None -> this.NotFound() :> IActionResult
