namespace PrDashboard.Server.Controllers.OAuthController

open System
open System.Security.Claims
open Microsoft.AspNetCore.Authentication
open Microsoft.AspNetCore.Authentication.Cookies
open Microsoft.AspNetCore.Mvc
open Bolero.Remoting.Server
open PrDashboard.Server.Types

[<ApiController>]
type OAuthController(getLoginUrl: Auth.GetLoginUrl, getTokenByCode: Auth.GetTokenByCode, saveToken: Cache.SaveToken) =
    inherit ControllerBase()
    let (Auth.GetLoginUrl _getLoginUrl) = getLoginUrl
    let (Auth.GetTokenByCode _getTokenByCode) = getTokenByCode
    let (Cache.SaveToken _saveToken) = saveToken
    
    [<HttpGet("/login")>]    
    member this.LogIn(org: string) = 
        let state = org
        let url = _getLoginUrl state
        this.Redirect url
    
    [<HttpPost("/logout")>]    
    member this.LogOut() = task{ 
        do! this.HttpContext.AsyncSignOut()
        return this.Ok("logged out")
    }
    
    [<HttpGet("/oauth/callback")>]    
    member this.Callback(code: string, state: string) = task{
        // save token
        let! token = _getTokenByCode code
        let sessionId = Guid.NewGuid().ToString()
        _saveToken sessionId token

        // create principal and sign user in
        let claims = [ Claim(CustomClaims.sessionId, sessionId) ]
        let principal = ClaimsPrincipal(ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme))
        do! this.HttpContext.SignInAsync(principal)
        
        // redirect back
        let org = state
        let path = "/" + org
        return this.Redirect path 
    }