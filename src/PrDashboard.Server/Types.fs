namespace PrDashboard.Server.Types

open System
open System.Threading.Tasks


type Project = {
    Id: Guid
    Name: string
}

type Repository = {
    Id: Guid
    WebUrl: string
}


[<CLIMutable>]
type OAuth ={
    ClientAppId: string
    ClientAppSecret: string
    Scope: string
    CallbackUrl: string
    AuthUrl: string
    TokenUrl: string
}

type Token = {
    AccessToken: string
    RefreshToken: string
}

type Avatar ={
    Bytes: byte array
}

module CustomClaims =
    let sessionId = "sessionId"
    let accessToken = "accessToken"
    let refreshToken = "refreshToken"

module Cache =
    type SaveToken = SaveToken of (string -> Token -> unit)
    type GetToken = GetToken of (string -> Token option)
    type SaveAvatar = SaveAvatar of (string -> Avatar -> unit)
    type GetAvatar = GetAvatar of (string -> Avatar option)
    
module Auth =
    type GetLoginUrl = GetLoginUrl of (string -> string)
    type GetTokenByCode = GetTokenByCode of (string -> Task<Token>)
    type RefreshToken = RefreshToken of (string -> Task<Token>)