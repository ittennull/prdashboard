module PrDashboard.Server.AuthenticatedExecution

open System
open System.Threading
open System.Threading.Tasks
open FsToolkit.ErrorHandling
open Microsoft.VisualStudio.Services.Common
open Microsoft.VisualStudio.Services.OAuth
open Microsoft.VisualStudio.Services.WebApi
open PrDashboard.Server.Types

let private createConnection organization (accessToken: string) =
    let uri = Uri $"https://dev.azure.com/{organization}"
    let credentials = VssCredentials(VssOAuthAccessTokenCredential accessToken)
    new VssConnection(uri, credentials)
    
let inline private runTask f = task{
    try
        let! result = f()
        return Ok result
    with
    | :? VssUnauthorizedException as exn ->
        return Error exn.Message
}

let private refreshTokenSemaphore = new SemaphoreSlim(1, 1)

let private refreshAndSaveToken getToken refreshToken saveToken sessionId token = task{
    let! acquired = refreshTokenSemaphore.WaitAsync(TimeSpan.FromSeconds 10.0)
    if acquired then
        try
            match getToken sessionId with
            | Some token' when token' <> token ->
                return! Ok token' |> Task.singleton
            | _ -> 
                return! runTask <| fun () -> task{
                    let! token = refreshToken token.RefreshToken
                    saveToken sessionId token
                    return token
                }
        finally
            refreshTokenSemaphore.Release() |> ignore
    else
        return! TaskResult.returnError "Couldn't acquire a lock"
}
    
let runDevOpsOperation
    (getToken: string -> Token option)
    (refreshToken: string -> Task<Token>)
    saveToken
    token
    sessionId
    organization
    f = taskResult{

    let run accessToken = task{
        use connection = createConnection organization accessToken
        return! f connection
    }
    
    let! result = runTask <| fun () -> run token.AccessToken
    match result with
    | Ok r -> return r
    | _ ->
        let! token' = refreshAndSaveToken getToken refreshToken saveToken sessionId token
        return! runTask <| fun () -> run token'.AccessToken
}