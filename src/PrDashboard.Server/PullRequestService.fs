module PrDashboard.Server.PullRequestService

open System
open System.Security.Cryptography
open System.Text
open System.Threading.Tasks
open Bolero.Remoting
open Bolero.Remoting.Server
open Microsoft.AspNetCore.Http
open Microsoft.Extensions.Caching.Memory
open Microsoft.Extensions.DependencyInjection
open Microsoft.TeamFoundation.Core.WebApi
open Microsoft.TeamFoundation.SourceControl.WebApi
open Microsoft.VisualStudio.Services.Graph.Client
open Microsoft.VisualStudio.Services.OAuth
open PrDashboard
open PrDashboard.Client.Pages.Dashboard.Types
open PrDashboard.Server.AuthenticatedExecution
open PrDashboard.Server.Types
open PrDashboard.Server.Types.Cache

type PullRequestData ={
    PullRequest: GitPullRequest
    CommentThreads: GitPullRequestCommentThread list
    AvatarId: string
    RepositoryUri: Uri
}

let getShortBranchName (branchName: string) =
    let start = "refs/heads/"
    match branchName.StartsWith start with
    | true -> branchName.[start.Length ..]
    | false -> branchName

let getPullRequestUrl (repositoryUri: Uri) (pullRequest: GitPullRequest) =
    Uri(repositoryUri.AbsoluteUri + "/pullrequest/" + pullRequest.PullRequestId.ToString())
    
let toPullRequest userId pullRequestData localAvatarId =
    let toPrStatus = function
        | -10s -> Rejected
        | -5s -> WaitForAuthor
        | 5s | 10s -> Approved
        | _ -> NoResponse

    let myVote =
        pullRequestData.PullRequest.Reviewers
        |> Seq.tryFind (fun x -> x.Id = userId)
        |> Option.map (fun x -> toPrStatus x.Vote)
        |> Option.defaultValue NoResponse
    
    let otherVote =
        pullRequestData.PullRequest.Reviewers
        |> Seq.filter (fun x -> x.Id <> userId && not x.IsContainer)
        |> Seq.map (fun x -> x.Vote)
        |> Seq.filter ((<>) 0s)
        |> Seq.fold min Int16.MaxValue
        |> toPrStatus
        
    let resolvedCommentThreads = pullRequestData.CommentThreads |> List.filter (fun x -> x.Status <> CommentThreadStatus.Unknown && x.Status <> CommentThreadStatus.Active)
    let totalThreads = pullRequestData.CommentThreads |> List.filter (fun x -> x.Status <> CommentThreadStatus.Unknown)
    
    let pr = pullRequestData.PullRequest
    {
        Id = pr.PullRequestId
        Title = pr.Title
        Uri = getPullRequestUrl pullRequestData.RepositoryUri pr
        MyVote = myVote
        OtherVote = otherVote
        CreatedAt = pr.CreationDate
        Project = pr.Repository.ProjectReference.Name
        Repository = pr.Repository.Name
        Branch = getShortBranchName pr.SourceRefName
        Author = pr.CreatedBy.DisplayName
        AuthorAvatarId = localAvatarId
        Feedback = { Resolved = resolvedCommentThreads.Length; Total = totalThreads.Length }
        IsDraft = pr.IsDraft.GetValueOrDefault()
    }
    
let loadProjects (cache: IMemoryCache) (projectClient: ProjectHttpClient) organization = 
    let rec loadProjects continuationToken = task{
        let continuationToken = match continuationToken with Some token -> token | _ -> null
        let! response = projectClient.GetProjects(Nullable(), Nullable(), Nullable(), null, continuationToken, Nullable())
        let projects = response |> List.ofSeq
        if response.ContinuationToken = null then
            return projects
        else
            let! nextProjects = loadProjects (Some response.ContinuationToken)
            return projects @ nextProjects
    }
    
    cache.GetOrCreateAsync(organization + ":projects", fun cacheEntry -> task{
        cacheEntry.AbsoluteExpirationRelativeToNow <- TimeSpan.FromMinutes 10.0 |> Nullable
        
        let! projects = loadProjects None
        return projects |> List.map (fun x -> {Id = x.Id; Name = x.Name})
    })

let loadRepositories (cache: IMemoryCache) (gitClient: GitHttpClient) (projectId: Guid) = 
    let key = sprintf "project:%O:repositories" projectId
    cache.GetOrCreateAsync(key, fun cacheEntry -> task{
        cacheEntry.AbsoluteExpirationRelativeToNow <- TimeSpan.FromMinutes 1.0 |> Nullable
        
        let! repos = gitClient.GetRepositoriesAsync(projectId)
        return repos
            |> Seq.toList
            |> List.map (fun x -> {Id = x.Id; WebUrl = x.WebUrl})
    })
    
let loadAvatar getAvatar saveAvatar (graphClient: GraphHttpClient) (avatarId: string) = task{
    let avatarId' =
        use hash = SHA256.Create()
        let avatarIdHash = hash.ComputeHash(Encoding.UTF8.GetBytes avatarId)
        (Convert.ToBase64String avatarIdHash).Replace("/", "_")
        
    match getAvatar avatarId' with
    | None ->
        try 
            let! avatar = graphClient.GetAvatarAsync avatarId
            saveAvatar avatarId' { Bytes = avatar.Value }
            return Some avatarId'
        with _ ->
            return None
    | Some _ ->
        return Some avatarId'
}

let loadPullRequestData cache (gitClient: GitHttpClient) showDrafts projectId = task{
    let loadPullRequestWithCommentThreads (repoId: Guid) = task{
        try
            let! prs = gitClient.GetPullRequestsAsync(repoId, GitPullRequestSearchCriteria())
            let prs = prs |> Seq.filter (fun pr -> showDrafts || not (pr.IsDraft.GetValueOrDefault())) |> Seq.toList
            
            let commentThreadsTasks = prs |> Seq.map (fun pr -> gitClient.GetThreadsAsync(repoId, pr.PullRequestId))
            let! commentThreads = Task.WhenAll(commentThreadsTasks)
            
            return commentThreads
                |> Seq.map Seq.toList
                |> Seq.zip prs
        with _ ->
            return Seq.empty
    }
    
    let! repos = loadRepositories cache gitClient projectId
    
    let tasks = repos |> Seq.map (fun repo -> loadPullRequestWithCommentThreads repo.Id)
    let! results = Task.WhenAll(tasks)
    return results
        |> Seq.collect id
        |> Seq.map (fun (pr, commentThreads) ->
              { PullRequest = pr
                CommentThreads = commentThreads
                AvatarId = pr.CreatedBy.Descriptor.ToString()
                RepositoryUri = repos |> List.find (fun x -> x.Id = pr.Repository.Id) |> fun x -> Uri(x.WebUrl) })
        |> Seq.toList
}

let getPullRequests
    (loadProjects: string -> Task<Project list>)
    (loadPullRequestData: bool -> Guid -> Task<PullRequestData list>)
    (loadAvatar: string -> Task<string option>)
    organization
    userId
    filters = task{
    
    let loadAvatars avatarIds = task{
        let loadAvatarTasks = avatarIds |> List.map loadAvatar
        let! newAvatarIds = Task.WhenAll loadAvatarTasks   
        return Seq.zip avatarIds newAvatarIds |> dict
    }
    
    let! pullRequestDataList = task{
        let! projects = loadProjects organization

        let projectFilter project =
            filters.Projects.IsEmpty || List.contains project.Name filters.Projects
        
        let tasks =
            projects
            |> List.filter projectFilter
            |> List.map (fun project -> loadPullRequestData filters.ShowDrafts project.Id)
        let! pullRequestDataList = Task.WhenAll(tasks)
        
        return pullRequestDataList |> Seq.collect id |> Seq.toList
    }
    
    let! avatarIdsMap =
        let avatarIds = pullRequestDataList |> List.map (fun x -> x.AvatarId) |> List.distinct
        loadAvatars avatarIds
    
    return pullRequestDataList
        |> List.map (fun pullRequestData ->
            let localAvatarId = avatarIdsMap.[pullRequestData.AvatarId]
            toPullRequest userId pullRequestData localAvatarId)
        |> List.sortByDescending (fun x -> x.CreatedAt)
}

let getProjects cache projectClient organization = task{
    let! projects = loadProjects cache projectClient organization
    return projects
        |> List.map (fun x -> x.Name)
        |> List.sort
}

type PullrequestService(ctx: IRemoteContext, cache: IMemoryCache) =
    inherit RemoteHandler<Client.Pages.Dashboard.Update.PullRequestService>()

    let runDevOpsOperation (httpContext: HttpContext) organization f =
        let sp = httpContext.RequestServices
        let (GetToken getToken) = sp.GetRequiredService<GetToken>()
        let (Auth.RefreshToken refreshToken) = sp.GetRequiredService<Auth.RefreshToken>()
        let (SaveToken saveToken) = sp.GetRequiredService<SaveToken>()
        let sessionId = httpContext.User.FindFirst(CustomClaims.sessionId).Value
        let accessDevopsToken = httpContext.User.FindFirst(CustomClaims.accessToken).Value
        let refreshDevopsToken = httpContext.User.FindFirst(CustomClaims.refreshToken).Value
        let token = {AccessToken = accessDevopsToken; RefreshToken = refreshDevopsToken}
        Async.AwaitTask <| task{
            match! runDevOpsOperation getToken refreshToken saveToken token sessionId organization f with
            | Ok r -> return r
            | _ -> return raise RemoteUnauthorizedException
        }
    
    override this.Handler = {
        getProjects = ctx.Authorize <| fun organization ->
            runDevOpsOperation ctx.HttpContext organization <| fun connection -> task{
                use projectClient = connection.GetClient<ProjectHttpClient>()
                return! getProjects cache projectClient organization
            }
        
        getPullRequests = ctx.Authorize <| fun (organization, filters) -> 
            runDevOpsOperation ctx.HttpContext organization <| fun connection -> task{
                use gitClient = connection.GetClient<GitHttpClient>()
                use projectClient = connection.GetClient<ProjectHttpClient>()
                use graphClient = connection.GetClient<GraphHttpClient>()
                let (GetAvatar getAvatar) = ctx.HttpContext.RequestServices.GetRequiredService<GetAvatar>()
                let (SaveAvatar saveAvatar) = ctx.HttpContext.RequestServices.GetRequiredService<SaveAvatar>()
                
                let loadProjects = loadProjects cache projectClient
                let loadAvatar = loadAvatar getAvatar saveAvatar graphClient
                let loadPullRequestData = loadPullRequestData cache gitClient
                let userId = connection.AuthenticatedIdentity.Id.ToString()
                return! getPullRequests loadProjects loadPullRequestData loadAvatar organization userId filters
            }
    }
