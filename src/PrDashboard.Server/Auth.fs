module PrDashboard.Server.Auth

open System.Net.Http
open System.Net.Http.Headers
open System.Text.Json
open System.Text.Json.Serialization
open PrDashboard.Server.Types

module DevOps = 
    [<CLIMutable>]
    type Token = {
        [<JsonPropertyName("access_token")>]
        AccessToken: string

        [<JsonPropertyName("token_type")>]
        TokenType: string

        [<JsonPropertyName("refresh_token")>]
        RefreshToken: string

        [<JsonPropertyName("expires_in")>]
        ExpiresIn: string
    }

    let getToken settings (httpClient: HttpClient) grantType assertion = task{
        use request = new HttpRequestMessage(HttpMethod.Post, settings.TokenUrl)
        request.Headers.Accept.Add(MediaTypeWithQualityHeaderValue("application/json"))
        request.Content <- new FormUrlEncodedContent(dict [
            "assertion", assertion 
            "grant_type", grantType
            "client_assertion_type", "urn:ietf:params:oauth:client-assertion-type:jwt-bearer" 
            "client_assertion", settings.ClientAppSecret
            "redirect_uri", settings.CallbackUrl
        ])
        
        use! response = httpClient.SendAsync(request)
        
        let! str = response.Content.ReadAsStringAsync()
        response.EnsureSuccessStatusCode() |> ignore
        
        let token = JsonSerializer.Deserialize<Token>(str)
        return {
            AccessToken = token.AccessToken
            RefreshToken = token.RefreshToken
        }
    }
    
let getLoginUrl settings state =
    $"{settings.AuthUrl}?client_id={settings.ClientAppId}&response_type=Assertion&state={state}&scope={settings.Scope}&redirect_uri={settings.CallbackUrl}"

let getTokenByCode settings httpClient code =
    DevOps.getToken settings httpClient "urn:ietf:params:oauth:grant-type:jwt-bearer" code

let refreshToken settings httpClient refreshToken =
    DevOps.getToken settings httpClient "refresh_token" refreshToken
