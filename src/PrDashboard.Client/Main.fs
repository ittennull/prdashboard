module PrDashboard.Client.Main

open Blazored.LocalStorage
open Elmish
open Bolero
open Bolero.Remoting
open Bolero.Remoting.Client
open Bolero.Templating.Client
open Pages.Dashboard.Update
open Pages.Model
open Pages.Types
open Pages.Update
open Pages.View
open Microsoft.Extensions.DependencyInjection

type MyApp() =
    inherit ProgramComponent<Model, Message>()

    override this.Program =
        let pullRequestService = this.Remote<PullRequestService>()
        let localStorage = this.Services.GetRequiredService<ILocalStorageService>()
        let navigateTo path forceLoad = this.NavigationManager.NavigateTo(path, forceLoad, false)
        let update = update navigateTo pullRequestService localStorage
        let router = Router.infer SetPage (fun model -> model.Page)
        Program.mkProgram (fun _ -> initModel, Cmd.none) update view
        |> Program.withRouter router
#if DEBUG
        |> Program.withHotReload
#endif