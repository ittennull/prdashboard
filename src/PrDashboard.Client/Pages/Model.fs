module PrDashboard.Client.Pages.Model

open Dashboard.Model
open Home.Model
open Types

type Model = {
    Page: Page
    Home: HomeModel
    Dashboard: DashboardModel
    Errors: string list
}

let initModel = {
    Page = Page.Home
    Home = initHomeModel
    Dashboard = initDashboardModel
    Errors = []
}
