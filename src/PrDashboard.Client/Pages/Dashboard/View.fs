module PrDashboard.Client.Pages.Dashboard.View

open Bolero.Html
open Model
open Types

let private class' = attr.``class``

let createPageTitle model =
    let title =
        match model.IsLoading with
        | true -> "Loading..."
        | false -> $"{model.PullRequests.Length} pull requests"
    h1 {class' "title is-size-4 is-marginless has-text-centered" ; text title}
    
let createPrSection pr =
    let icon isMyVote vote =
        let icon classes =
            let modifiers = if isMyVote then "is-medium mdi-24px" else "mdi-dark" 
            span {class' $"icon ml-3 mdi {modifiers} {classes}"}
            
        cond vote <| function
        | NoResponse -> empty()
        | Rejected -> icon "mdi-cancel has-text-danger"
        | WaitForAuthor -> icon "mdi-account-clock wait-for-author-icon"
        | Approved -> icon "mdi-checkbox-marked-circle has-text-success"
        
    let feedback =
        match pr.Feedback.Resolved, pr.Feedback.Total with
        | x, y when x = y -> empty()
        | resolved, total ->
            span{
                span {class' "has-text-grey-light inline-text-margin" ; text "feedback: "}
                text $"{resolved}/{total}"
            }
    
    div{
        class' "pr-section columns is-variable is-2"
        div {
            class' "column is-narrow"
            figure {
                class' "image is-32x32"
                cond pr.AuthorAvatarId <| function
                | Some avatarId -> img {attr.src $"/avatar/{avatarId}"}
                | None -> empty()
            }
        }
        
        div {
            class' "column py-0"
            div {
                class' "level is-marginless"
                div {
                    class' "level-left"
                    let classes = "is-size-5"
                    let classes = if pr.IsDraft then "has-text-grey-light " + classes else classes
                    let title = sprintf "%s%s" (if pr.IsDraft then "[Draft] " else "") pr.Title
                    span {class' "has-text-grey-light mr-2"; text $"{pr.Id}: "}
                    a {
                        class' classes
                        attr.href (pr.Uri.ToString())
                        text title
                    }
                }
                div {
                    class' "level-right"
                    feedback
                    icon true pr.MyVote
                    icon false pr.OtherVote
                }
            }
            div {
                class' "level is-marginless"
                div {
                    class' "level-left"
                    text $"{pr.Repository}: "
                    span {class' "has-text-weight-bold inline-text-margin" ; text pr.Branch}
                    span {class' "has-text-grey-light inline-text-margin" ; text "in"}
                    text pr.Project
                }
                div {
                    class' "level-right"
                    span {class' "has-text-grey-light inline-text-margin" ; text "by"}
                    text pr.Author
                    span {class' "has-text-grey-light inline-text-margin" ; text "on"}
                    text <| pr.CreatedAt.ToString("dd MMM")
                }
            }
            hr {class' "is-marginless"}
        }
    }
    
let checkbox withText isChecked onChange =
    label {
        class' "checkbox"
        input {
            attr.``type`` "checkbox"
            attr.``checked`` isChecked
            on.change onChange
        }
        text withText }
    
let createProjectFiltersSection model dispatch =
    let caption =
        match model.Filters.Projects.Length with
        | 0 -> "Showing all projects"
        | 1 -> "Showing 1 project"
        | n -> $"Showing {n} projects"
        
    div {
        text caption
        
        button {
            class' "button is-small is-info is-outlined ml-2"
            on.click (fun _ -> dispatch ToggleFilters)
            text "Filters"
        }
        
        if model.ShowProjectFilter then
            let projectFilterItem name =
                let isChecked = model.Filters.Projects |> List.contains name
                div {
                    checkbox name isChecked (fun _ -> dispatch (ToggleProjectFilter name))
                }
                
            let showDraftsItem =
                checkbox "Show draft PRs" model.Filters.ShowDrafts (fun _ -> dispatch ToggleShowDraftPrs)
                
            let saveButton =
                button {
                    class' "button is-small is-success is-outlined filter-button"
                    on.click (fun _ -> dispatch SaveFilters)
                    text "Save"
                } 
                
            div {
                class' "columns"
                div {
                    class' "column is-narrow"
                    forEach model.AllProjects projectFilterItem
                }
                div {
                    class' "column"
                    showDraftsItem
                }
            }
            div {
                class' "has-text-centered"
                saveButton
            }
            
        hr {empty()}
    }
    
let dashboardView model dispatch =
    let title = createPageTitle model
    let projectFilters = createProjectFiltersSection model dispatch
    let pullRequests = forEach model.PullRequests createPrSection
    
    div {
        title
        projectFilters
        pullRequests
    }