module PrDashboard.Client.Pages.Dashboard.Types

open System

type PrStatus = NoResponse | Approved | Rejected | WaitForAuthor

type Feedback = {
    Resolved: int
    Total: int
}

type PullRequest ={
    Id: int
    Title: string
    Uri: Uri
    MyVote: PrStatus
    OtherVote: PrStatus
    CreatedAt: DateTime
    Project: string
    Repository: string
    Branch: string
    Author: string
    AuthorAvatarId: string option
    Feedback: Feedback
    IsDraft: bool
}

type Filters ={
    Projects: string list
    ShowDrafts: bool
}

type DashboardMessage =
    | OpenPage of org: string
    | GetPullRequests
    | GotPullRequests of PullRequest list
    | GetProjects
    | GotProjects of string list
    | ToggleFilters
    | ToggleProjectFilter of string
    | ToggleShowDraftPrs
    | LoadFilters
    | FiltersLoaded of Filters
    | SaveFilters
    | Error of exn