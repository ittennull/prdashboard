module PrDashboard.Client.Pages.Dashboard.Update

open Blazored.LocalStorage
open Elmish
open Bolero.Remoting
open Bolero.Remoting.Client
open Model
open Types

type PullRequestService =
    {
        getProjects: string -> Async<string list>
        getPullRequests: string*Filters -> Async<PullRequest list>
    }
    interface IRemoteService with
        member this.BasePath = "/prs"
        
module private MessageHandlers =
    let projectsFilterKey org = $"{org}:ProjectsFilter"
    let showDraftsKey org = $"{org}:ShowDrafts"
    
    let openPage model org =
        let model' = {model with Organization = Some org} 
        let cmd = Cmd.batch [Cmd.ofMsg GetProjects; Cmd.ofMsg LoadFilters]
        model', cmd
        
    let getPullRequests pullRequestService model =
        let (Some org) = model.Organization
        let cmd = Cmd.OfAsync.either pullRequestService.getPullRequests (org, model.Filters) GotPullRequests Error
        {model with IsLoading = true}, cmd
        
    let gotPullRequests model prs =
        { model with PullRequests = prs; IsLoading = false }, Cmd.none
        
    let getProjects pullRequestService model =
        let (Some org) = model.Organization
        let cmd = Cmd.OfAsync.either pullRequestService.getProjects org GotProjects Error
        model, cmd
        
    let gotProjects model projects =
        { model with AllProjects = projects }, Cmd.none
        
    let toggleFilters model =
        { model with ShowProjectFilter = not model.ShowProjectFilter }, Cmd.none
        
    let toggleProjectFilter model name =
        let projects =
            if List.contains name model.Filters.Projects then model.Filters.Projects |> List.filter ((<>) name)
            else name :: model.Filters.Projects
        { model with Filters = {model.Filters with Projects = projects}}, Cmd.none
        
    let toggleShowDraftPrs model =
        { model with Filters = {model.Filters with ShowDrafts = not model.Filters.ShowDrafts}}, Cmd.none
        
    let loadFilters (localStorage: ILocalStorageService) model =
        let (Some org) = model.Organization
        let load() = task{
            let! projects = localStorage.GetItemAsync<string array>(projectsFilterKey org)
            let! showDrafts = localStorage.GetItemAsync<bool>(showDraftsKey org)
            let projects = match projects with null -> [] | x -> List.ofArray x
            return {Projects = projects; ShowDrafts = showDrafts}
        }
        let cmd = Cmd.OfTask.either load () FiltersLoaded Error
        model, cmd
        
    let filtersLoaded model filters =
        { model with Filters = filters }, Cmd.ofMsg GetPullRequests
        
    let saveFilters (localStorage: ILocalStorageService) model =
        let (Some org) = model.Organization
        let save() = task{
            do! localStorage.SetItemAsync(projectsFilterKey org, model.Filters.Projects)
            do! localStorage.SetItemAsync(showDraftsKey org, model.Filters.ShowDrafts)
        }
        let cmd = Cmd.OfTask.either save () (fun _ -> GetPullRequests) Error
        { model with ShowProjectFilter = false }, cmd
        
    let onRemoteUnauthorizedException navigateTo model =
        let (Some org) = model.Organization
        navigateTo $"/login?org={org}" true
        model, Cmd.none
            
open MessageHandlers

let dashboardUpdate navigateTo pullRequestService (localStorage: ILocalStorageService) message model =
    match message with
    | OpenPage org -> openPage model org |> Ok
    | GetPullRequests -> getPullRequests pullRequestService model |> Ok
    | GotPullRequests prs -> gotPullRequests model prs |> Ok
    | GetProjects -> getProjects pullRequestService model |> Ok
    | GotProjects projects -> gotProjects model projects |> Ok 
    | ToggleFilters -> toggleFilters model |> Ok
    | ToggleProjectFilter name -> toggleProjectFilter model name |> Ok
    | ToggleShowDraftPrs -> toggleShowDraftPrs model |> Ok
    | LoadFilters -> loadFilters localStorage model |> Ok
    | FiltersLoaded filters -> filtersLoaded model filters |> Ok
    | SaveFilters -> saveFilters localStorage model |> Ok
    | Error ex when (ex.GetBaseException() :? RemoteUnauthorizedException) -> onRemoteUnauthorizedException navigateTo model |> Ok
    | Error ex -> Result.Error ex
