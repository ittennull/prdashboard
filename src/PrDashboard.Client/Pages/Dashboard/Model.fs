module PrDashboard.Client.Pages.Dashboard.Model

open Types

type DashboardModel = {
    Organization: string option
    PullRequests: PullRequest list
    ShowProjectFilter: bool
    AllProjects: string list
    Filters: Filters
    IsLoading: bool
}

let initDashboardModel = {
    Organization = None
    PullRequests = []
    ShowProjectFilter = false
    AllProjects = []
    Filters = {
        Projects = []
        ShowDrafts = false
    }
    IsLoading = false
}