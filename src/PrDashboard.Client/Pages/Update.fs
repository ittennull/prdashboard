module PrDashboard.Client.Pages.Update

open Blazored.LocalStorage
open Elmish
open Bolero.Remoting.Client
open Dashboard.Types
open Dashboard.Update
open Home.Update
open Model
open PrDashboard.Client.Pages.Home.Types
open Types

module private MessageHandlers =
    let onHomeMessage model msg =
        match msg with
        | GoToDashboard org ->
            model, Cmd.ofMsg (SetPage (Page.Dashboard org))
        | msg ->
            let m,c = homeUpdate msg model.Home
            {model with Home = m}, c
        
    let onDashboardMessage navigateTo pullRequestService (localStorage: ILocalStorageService) model msg =
        let result = dashboardUpdate navigateTo pullRequestService localStorage msg model.Dashboard
        match result with
        | Result.Ok (m,c) -> {model with Dashboard = m}, c |> Cmd.map Dashboard
        | Result.Error ex -> model, Cmd.ofMsg (Error ex)
        
    let setPage model page =
        if model.Page = page then
            model, Cmd.none
        else
            let cmd =
                match page with
                | Page.Home -> Cmd.none
                | Page.Dashboard org -> OpenPage org |> Dashboard |> Cmd.ofMsg
                    
            { model with Page = page }, cmd
        
    let onError model (exn: exn) =
        { model with Errors = exn.GetBaseException().Message :: model.Errors }, Cmd.none
        
    let clearError model =
        { model with Errors = [] }, Cmd.none
        
open MessageHandlers

let update navigateTo pullRequestService localStorage message (model: Model) =
    match message with
    | Home msg -> onHomeMessage model msg
    | Dashboard msg -> onDashboardMessage navigateTo pullRequestService localStorage model msg
    | SetPage page -> setPage model page
    | Error exn -> onError model exn
    | ClearError -> clearError model
