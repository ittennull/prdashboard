module PrDashboard.Client.Pages.View

open Bolero
open Bolero.Html
open Model
open Types
open Home.View
open Dashboard.View

let private class' = attr.``class``

let view model dispatch : Node =
    let page =
        cond model.Page <| function
        | Page.Home -> homeView model.Home (Home >> dispatch)
        | Page.Dashboard _ -> dashboardView model.Dashboard (Dashboard >> dispatch)
        
    let errorNotification =
        cond model.Errors.IsEmpty <| function
        | true -> empty()
        | false -> 
            div {
                class' "notification is-danger is-light"
                button {
                    class' "delete"
                    on.click (fun _ -> dispatch ClearError)
                }
                
                forEach model.Errors <| (fun error -> div{text error})
            }
        
    div {
        errorNotification
        page
    }