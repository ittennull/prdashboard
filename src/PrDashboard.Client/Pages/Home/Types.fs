module PrDashboard.Client.Pages.Home.Types

type HomeMessage =
    | SetOrganization of string
    | GoToDashboard of string