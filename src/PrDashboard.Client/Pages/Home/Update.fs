module PrDashboard.Client.Pages.Home.Update

open Elmish
open Bolero.Remoting.Client
open Model
open Types

module private MessageHandlers =
    let setOrganization model org =
        { model with Organization = org }, Cmd.none
        
open MessageHandlers

let homeUpdate message model =
    match message with
    | SetOrganization org -> setOrganization model org
    | GoToDashboard _ -> failwith "GoToDashboard messages is not expected here"
