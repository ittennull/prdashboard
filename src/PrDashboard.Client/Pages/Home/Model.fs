module PrDashboard.Client.Pages.Home.Model

type HomeModel = {
    Organization: string
}

let initHomeModel = {
    Organization = ""
}