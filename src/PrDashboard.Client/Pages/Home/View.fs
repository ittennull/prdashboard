module PrDashboard.Client.Pages.Home.View

open Bolero.Html
open Types
open PrDashboard.Client.Pages.Home.Model

let private class' = attr.``class``

let homeView model dispatch =
    div{
        div{
            class' "field"
            label{
                class' "label"
                text "Your organization"
            }
        }
        div{
            class' "field has-addons"
            div{
                class' "control"
                input {
                    class' "input"
                    attr.``type`` "text"
                    attr.placeholder "organization"
                    on.change (fun e -> unbox e.Value |> SetOrganization |> dispatch)
                }
            }
            div{
                class' "control"
                button {
                    class' "button is-info"
                    on.click (fun _ -> dispatch (GoToDashboard model.Organization))
                    text "Set"
                } 
            }
        }
        p{
            class' "help"
            span {
                class' "has-text-grey-light"
                text "You can find it in URL: https://dev.azure.com/"
            }
            span {
                class' "has-text-black"
                text "{organization}"
            }
        }
    }