module PrDashboard.Client.Pages.Types

open Dashboard.Types
open Home.Types
open Bolero

type Page =
    | [<EndPoint "/">] Home
    | [<EndPoint "/{org}">] Dashboard of org:string

type Message =
    | Home of HomeMessage
    | Dashboard of DashboardMessage
    | SetPage of Page
    | Error of exn
    | ClearError