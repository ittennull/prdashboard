FROM mcr.microsoft.com/dotnet/sdk:7.0-alpine
WORKDIR /build
COPY . .
RUN dotnet publish src/PrDashboard.Server/PrDashboard.Server.fsproj --output "/output" --configuration Release


FROM mcr.microsoft.com/dotnet/aspnet:7.0-alpine
WORKDIR /app
COPY --from=0 /output .
ENTRYPOINT dotnet PrDashboard.Server.dll